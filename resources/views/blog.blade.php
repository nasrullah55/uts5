@extends('layouts.main')

@section('container')
<button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal" data-bs-target="#tambahpost">
    Buat Post
</button>
<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
    <div class="col">
        <div class="card" style="width: 22rem;">
            <img src=" {{ asset('img/1.jpg') }} " class="card-img-top" alt="...">
            <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-primary">Selengkapnya</a>
            <a href="#" class="btn btn-success">Edit</a>
            <a href="#" class="btn btn-danger">Hapus</a>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card" style="width: 22rem;">
            <img src=" {{ asset('img/1.jpg') }} " class="card-img-top" alt="...">
            <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-primary">Selengkapnya</a>
            <a href="#" class="btn btn-success">Edit</a>
            <a href="#" class="btn btn-danger">Hapus</a>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card" style="width: 22rem;">
            <img src=" {{ asset('img/1.jpg') }} " class="card-img-top" alt="...">
            <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-primary">Selengkapnya</a>
            <a href="#" class="btn btn-success">Edit</a>
            <a href="#" class="btn btn-danger">Hapus</a>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card" style="width: 22rem;">
            <img src=" {{ asset('img/1.jpg') }} " class="card-img-top" alt="...">
            <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-primary">Selengkapnya</a>
            <a href="#" class="btn btn-success">Edit</a>
            <a href="#" class="btn btn-danger">Hapus</a>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card" style="width: 22rem;">
            <img src=" {{ asset('img/1.jpg') }} " class="card-img-top" alt="...">
            <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-primary">Selengkapnya</a>
            <a href="#" class="btn btn-success">Edit</a>
            <a href="#" class="btn btn-danger">Hapus</a>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card" style="width: 22rem;">
            <img src=" {{ asset('img/1.jpg') }} " class="card-img-top" alt="...">
            <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-primary">Selengkapnya</a>
            <a href="#" class="btn btn-success">Edit</a>
            <a href="#" class="btn btn-danger">Hapus</a>
            </div>
        </div>
    </div>
    
    
</div>

<!-- Modal -->
    <div class="modal fade" id="tambahpost" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Judul Post</label>
                    <input type="judul" class="form-control" id="judul" name="judul" placeholder="judul post">
                </div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Pilih Gambar</label>
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label">Example textarea</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        </div>
    </div>
    
    
@endsection